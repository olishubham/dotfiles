--[[
-- terraform.lua - vim terraform plugin configuration
--
--]]


-- set and lets
vim.cmd([[let g:terraform_fmt_on_save=1]])
vim.cmd([[let g:terraform_align=1]])

